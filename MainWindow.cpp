#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QFileDialog>
#include <C:/boost/boost/make_shared.hpp>

#include "QChartExt.h"
#include "QChartViewExt.h"
#include "QLineSeriesExt.h"
#include "QScatterSeriesExt.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    _poAxisX(new QDateTimeAxis),
    _poMainChart(new QChartViewer),
    ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  this->vInit();
}
//-------------------------------------------------------------------------------------------------
MainWindow::~MainWindow()
{
  delete ui;
}
//Инициализация графика
void MainWindow::vInit()
{
  ui->ptrChartLayout->addWidget(_poMainChart->pGetRootWidget());

 // Set chat inside setting
  _poMainChart->vSetAxesTitle("Дата, T","Давление, P");

  // Set default chart scale
  vSetRange(-50,50,-100,100);

  // Set setting for axis X
  //_poAxisX->setTickCount(10);
  //_poAxisX->setTitleText("<H4>Дата, T</H3>");
  _poAxisX->setFormat("d MMM yyyy");

}
//-------------------------------------------------------------------------------------------------
void MainWindow::on_ptrButtonAppendData_clicked()
{
   QString strCurrentFile = QFileDialog::getOpenFileName(this, tr("Open csv file with data"), "\\", tr("База (*.csv)"));
   QFile fFile(strCurrentFile);

   // Check exist
   if(fFile.exists()){
   // Clear vector with data
   _vecUserData.clear();
   _mapDate.clear();
   if(fFile.open(QIODevice::ReadOnly)){
     QTextStream inStream(&fFile);

     while (!inStream.atEnd()) {

      auto stringList = inStream.readLine().split(";");

      QDateTime momentInTime;
      momentInTime.setDate(QDate(stringList[2].toInt(), stringList[1].toInt(),stringList[0].toInt()));

      _vecUserData.append(std::make_pair(momentInTime,stringList.last().toDouble()));
      _mapDate.insert(momentInTime.toMSecsSinceEpoch(),stringList.last().toDouble());
     }

     _poMainChart->pGetPtrChartViewExt()->vSetMapDate(_mapDate);

     ui->ptrButtonShowChart->setEnabled(true);
     ui->ptrDataBegin->setEnabled(true);
     ui->ptrDataEnd->setEnabled(true);
     ui->ptrSbPeriod->setEnabled(true);

     ui->ptrDataBegin->setMaximumDateTime(_vecUserData.last().first);
     ui->ptrDataBegin->setMinimumDateTime(_vecUserData.begin()->first);
     ui->ptrDataBegin->setDate(_vecUserData.begin()->first.date());

     ui->ptrDataEnd->setMaximumDateTime(_vecUserData.last().first);
     ui->ptrDataEnd->setMinimumDateTime(_vecUserData.begin()->first);
     ui->ptrDataEnd->setDate(_vecUserData.last().first.date());

     }
   }

  MainWindow::on_ptrButtonShowChart_clicked();
}
//-------------------------------------------------------------------------------------------------
void MainWindow::on_ptrButtonShowChart_clicked()
{
  _poMainChart->vRemoveAll();
  _poMainChart->pGetPtrChartExt()->axisX()->hide();

  int iSoundPeriod = ui->ptrSbPeriod->text().toInt();

  boost::shared_ptr<QtCharts::QLineSeriesExt> ptrLineChart(boost::make_shared<QtCharts::QLineSeriesExt>());
  boost::shared_ptr<QtCharts::QScatterSeriesExt> ptrPointChart(boost::make_shared<QtCharts::QScatterSeriesExt>());

  for(int i(0);i< _vecUserData.size();i+=iSoundPeriod) {
    if(_vecUserData[i].first.date() >= ui->ptrDataBegin->date() && _vecUserData[i].first.date() <=ui->ptrDataEnd->date()){
      ptrLineChart.get()->append(_vecUserData[i].first.toMSecsSinceEpoch(),_vecUserData[i].second);
      ptrPointChart.get()->append(_vecUserData[i].first.toMSecsSinceEpoch(),_vecUserData[i].second);
    }
  }


  int iID = _poMainChart->iAddSeries(ptrLineChart);
  ptrLineChart->poGetSeriesParam()->vSetID(uint(iID));

  ptrLineChart.get()->setPen(QPen(QColor("#aa0000"),2));

  iID = _poMainChart->iAddSeries(ptrPointChart);
  ptrPointChart->poGetSeriesParam()->vSetID(uint(iID));

  ptrPointChart.get()->setMarkerSize(10);

  _poMainChart->pGetPtrChartExt()->addAxis(_poAxisX, Qt::AlignBottom);

  ptrPointChart.get()->attachAxis(_poAxisX);

  _poMainChart->pGetPtrChartExt()->zoomReset();

}
//-------------------------------------------------------------------------------------------------
void MainWindow::vSetRange(int iMinX,int iMaxX,int iMinY,int iMaxY)
{
  _poMainChart->vSetRange(iMinY,iMaxY,iMinX,iMaxX);

  boost::shared_ptr<QtCharts::QScatterSeriesExt>poScale(boost::make_shared<QtCharts::QScatterSeriesExt>());
  poScale.get()->append(iMinY,iMinX);
  poScale.get()->append(iMaxY,iMaxX);
  poScale->setMarkerSize(1);

  int iScaleID = _poMainChart->iAddSeries(poScale);
  poScale->poGetSeriesParam()->vSetID(uint(iScaleID));
}
//-------------------------------------------------------------------------------------------------
void MainWindow::on_ptrDataBegin_dateChanged(const QDate &date)
{
  if(date < ui->ptrDataEnd->date()){
    MainWindow::on_ptrButtonShowChart_clicked();
  }
}
//-------------------------------------------------------------------------------------------------
void MainWindow::on_ptrDataEnd_dateChanged(const QDate &date)
{
  if(date > ui->ptrDataBegin->date()){
    MainWindow::on_ptrButtonShowChart_clicked();
  }
}
//-------------------------------------------------------------------------------------------------
void MainWindow::on_ptrSbPeriod_valueChanged(int arg1)
{
  MainWindow::on_ptrButtonShowChart_clicked();

  Q_UNUSED(arg1)
}
//-------------------------------------------------------------------------------------------------
