QT       += core gui printsupport

!include( External/QChartViewer/QChartViewer.pri ) {
    error( "Couldn't find the QChartViewer.pri file!" )
}
#-------------------------------------------------------------------------------------------------
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


VERSION = 1
TARGET = QCustomPlotDateTime
TEMPLATE = app
APP_DIR  = "app(Qt_$$QT_VERSION)"
CONFIG  += thread c++11

SOURCES += Main.cpp\
        Mainwindow.cpp

HEADERS  += MainWindow.h

FORMS    += MainWindow.ui

DEFINES += _CONVERTS_BYTE_ORDER # _CONVERTS_BYTE_ORDER - переворот байт в NetCoDecBuf

CONFIG(release, debug|release){
  DEFINES  += NDEBUG # NDEBUG - не выводить сообщений assert()
  QT_LIB    = Qt5Core.dll Qt5Gui.dll Qt5Sql.dll Qt5Widgets.dll Qt5Charts.dll Qt5PrintSupport.dll
  QT_PLIB   = platforms\qwindows.dll sqldrivers\qsqlibase.dll
} else {
  QMAKE_CXXFLAGS_DEBUG += -pg
  QMAKE_LFLAGS_DEBUG += -pg
  TARGET    = $$join(TARGET,,,D)
  QT_LIB    = Qt5Cored.dll Qt5Guid.dll Qt5Sqld.dll Qt5Widgetsd.dll Qt5Chartsd.dll Qt5PrintSupportd.dll
  QT_PLIB   = platforms\qwindowsd.dll sqldrivers\qsqlibased.dll
}


QT_LIB   += libstdc++-6.dll libwinpthread-1.dll libgcc_s_dw2-1.dll
DESTDIR   = $$OUT_PWD/$$APP_DIR/$$TARGET


LIBS +=  \
    libws2_32 \
    $$PWD/External/Libs/libboost_coroutine-mgw53-mt-x32-1_66.a \
    $$PWD/External/Libs/libboost_system-mgw53-mt-x32-1_66.a \
    $$PWD/External/Libs/libboost_context-mgw53-mt-x32-1_66.a

windows:{
  DEFINES += WIN32
  RC_ICONS = Plotter.ico
  QT_DIR_LIB      = $$dirname(QMAKE_QMAKE)
  QT_DIR_LIB      = $$replace(QT_DIR_LIB,/,\\)
  WIN_APP_DIR     = $$replace(OUT_PWD,/,\\)\\$$APP_DIR\\$$TARGET
#  QMAKE_POST_LINK = "xcopy /E /Y \"$$replace(PWD,/,\\)\\res\\lang\\*\" \"$$WIN_APP_DIR\\lang\\\""
  QMAKE_POST_LINK = "(date /t & time /t) > \"$$WIN_APP_DIR\\build.date\""
  for(DLL,QT_LIB):{
    exists($$dirname(QMAKE_QMAKE)/$$DLL){
      QMAKE_POST_LINK += & "copy /y \"$$QT_DIR_LIB\\$$DLL\" \"$$WIN_APP_DIR\\\""
    }
  }
  for(DLL,QT_PLIB):{
    exists($$dirname(QMAKE_QMAKE)/../plugins/$$DLL){
      QMAKE_POST_LINK += & "echo F | xcopy /E /Y \"$$QT_DIR_LIB\\..\\plugins\\$$DLL\" \"$$WIN_APP_DIR\\$$DLL\""
    }
  }

}
