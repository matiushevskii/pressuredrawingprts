#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDate>
#include <QMainWindow>
#include <QDateTimeAxis>
#include "QChartViewer.h"


QT_CHARTS_USE_NAMESPACE

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
  /** Конструктор по-умолчанию*/
  explicit MainWindow(QWidget *parent = 0);

  /** Деструктор*/
  ~MainWindow();

  /** Инициализация*/
  inline void vInit();

private slots:
  /** Обработчик клавиши Добавить*/
  void on_ptrButtonAppendData_clicked();

  /** Обработчик клавиши Перестроить*/
  void on_ptrButtonShowChart_clicked();

  void on_ptrDataBegin_dateChanged(const QDate &date);

  void on_ptrDataEnd_dateChanged(const QDate &date);

  void on_ptrSbPeriod_valueChanged(int arg1);

private:
  /** Начальный масштаб*/
  inline void vSetRange(int iMinX,int iMaxX,int iMinY,int iMaxY);

  /** Временная ось*/
  QDateTimeAxis *_poAxisX;

  /** График*/
  QChartViewer *_poMainChart;

  /** Контейнер с данными*/
  QVector<std::pair<QDateTime,double>> _vecUserData;

  /** Контейнер с секундами */
  QMap<qint64,double> _mapDate;

  /** Указатель на-форму*/
  Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
